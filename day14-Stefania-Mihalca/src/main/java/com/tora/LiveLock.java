package com.tora;

public class LiveLock {
    public static void main(String[] args) {
        Object left = new Object();
        Object right = new Object();
        Person person1 = new Person(left, right, 0);
        Person person2 = new Person(right, left, 1);
        person1.setOther(person2);
        person2.setOther(person1);
        person1.start();
        person2.start();
    }
}

class Person extends Thread {
    private final Object left;
    private final Object right;
    private Person other;
    private Object current;

    Person(Object left, Object right, int firstDirection) {
        this.left = left;
        this.right = right;
        if (firstDirection == 0) {
            current = left;
        } else {
            current = right;
        }
    }

    void setOther(Person person) {
        other = person;
    }

    Object getDirection() {
        return current;
    }

    Object getOppositeDirection() {
        if (current.equals(left)) {
            return right;
        } else {
            return left;
        }
    }

    void switchDirection() throws InterruptedException {
        Thread.sleep(100);
        current = getOppositeDirection();
        System.out.println(Thread.currentThread().getName() + " is stepping aside.");
    }

    public void run() {
        while (getDirection().equals(other.getDirection())) {
            try {
                switchDirection();
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
    }
}
