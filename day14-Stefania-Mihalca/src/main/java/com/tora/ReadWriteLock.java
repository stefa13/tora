package com.tora;

import java.util.ArrayList;
import java.util.List;

public class ReadWriteLock {
    private int readers = 0;
    private int writers = 0;
    private int writeRequests = 0;
    private final List<Thread> threadsRead = new ArrayList<>();
    private final List<Thread> threadsWrite = new ArrayList<>();


    public synchronized void lockRead() throws InterruptedException {
        threadsRead.add(Thread.currentThread());

        while (writers > 0 || writeRequests > 0) {
            wait();
        }
        readers++;
    }

    public synchronized void lockWrite() throws InterruptedException {
        threadsWrite.add(Thread.currentThread());

        writeRequests++;

        while (readers > 0 || writers > 0) {
            wait();
        }
        writeRequests--;
        writers++;
    }

    public synchronized void unlockRead() {
        if (threadsRead.contains(Thread.currentThread())) {
            readers--;
            threadsRead.remove(Thread.currentThread());
            notifyAll();
        }
    }

    public synchronized void unlockWrite() {
        if (threadsWrite.contains(Thread.currentThread())) {
            writers--;
            threadsWrite.remove(Thread.currentThread());
            notifyAll();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int count = 1;
        ReadWriteLock readWriteLock = new ReadWriteLock();
        readWriteLock.lockRead();
        int copy = count;
        readWriteLock.unlockRead();
        readWriteLock.lockWrite();
        int c = count + 1;
        System.out.println(count);
    }

}
