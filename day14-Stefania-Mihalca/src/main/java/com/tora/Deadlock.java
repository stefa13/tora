package com.tora;

public class Deadlock {
    public static void main(String[] args) {
        Object resource1 = new Object();
        Object resource2 = new Object();

        // first thread tries to lock resource1 then resource2
        // then second thread tries to lock the resources in the opposite order

        Thread thread1 = new Thread(() -> {
            synchronized (resource1) {
                System.out.println("Thread1: Locked resource1");

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }

                synchronized (resource2) {
                    System.out.println("Thread1: Locked resource2");
                }
            }
        });

        // second thread
        Thread thread2 = new Thread(() -> {
            synchronized (resource2) {
                System.out.println("Thread2: Locked resource2");

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }

                synchronized (resource1) {
                    System.out.println("Thread2: Locked resource1");
                }
            }
        });

        thread1.start();
        thread2.start();
    }
}
