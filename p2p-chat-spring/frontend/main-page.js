const options = document.getElementById("options");
const sendButton = document.getElementById("send-button");
const addMemberButton = document.getElementById("addMember-button");
const userName = document.getElementById("username-field");
const messageField = document.getElementById("message-field");
const groupMember = document.getElementById("groupMember-field");
const byeByeButton = document.getElementById("byebye-button");

let groupMembers = [];

sendButton.addEventListener("click", () => {
    let option = options.value;
    let user = userName.value;

    let baseUrl = "http://localhost:8080/";
    let url = "http://localhost:8080/" + option + "/" + user;


    if (option === "group-hello") {
        groupHello(url);
    } else if (option === "send") {
        sendMessage(url);
    } else if (option === "messages") {
        listMessages(baseUrl, user);
    } else {
        $.ajax({
            type: 'POST',
            url: url
        });
    }
    listGroupMembers();
    groupMembers = [];
})

function sendMessage(url) {
    const messageText = messageField.value;
    console.log("Message is: " + messageText);
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: url,
        data: JSON.stringify(messageText)
    });
}

function groupHello(url) {
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: url,
        data: JSON.stringify(groupMembers)
    });
}

function listMessages(baseUrl, user) {

    let url = baseUrl + "messages/" + user;
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'GET',
        url: url,
        success: function (data) {
            console.log(data);
            populateMessageBox(data);
        }
    });
}

addMemberButton.addEventListener("click", () => {
    groupMemberName = groupMember.value;
    console.log("Added member: " + groupMemberName)
    groupMembers.push(groupMemberName);
    listGroupMembers();
})

byeByeButton.addEventListener("click", () => {
    let url = "http://localhost:8080/byebye";
    $.ajax({
        type: 'POST',
        url: url
    });
})

function populateMessageBox(messages) {
    let htmlContent = "<ul>";

    for (message of messages) {
        htmlContent += "<li>" + " from: " + message.from + " to: " + message.to + " content: " + message.content + "</li>";
    }
    htmlContent += "</ul>";
    document.getElementById("message-box").innerHTML = htmlContent;
}

function listGroupMembers() {
    let htmlContent = "<h4> Group members</h4>"
    htmlContent += "<ul title='Group Members'>";

    for (member of groupMembers) {
        htmlContent += "<li>" + member + "</li>";
    }
    htmlContent += "</ul>";
    document.getElementById("group-members").innerHTML = htmlContent;
}
