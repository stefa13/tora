package com.tora.service;

import com.rabbitmq.client.*;
import com.tora.message.Message;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

@Service
public class PeerService {

    private final static String URI = "amqps://qyeginpd:8g5qG8BICI4JxCwU7KmmqmCJJ5g_rVNs@rat.rmq2.cloudamqp.com/qyeginpd";
    private final String EXCHANGE_NAME = "tora";
    private final Channel channel;
    private final ExecutorService executorService;
    private final Set<String> requestsSent = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Set<String> requestsReceived = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Set<String> friends = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Set<String> groups = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Set<String> groupRequests = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Set<Message> messages = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private String myUserName;

    @Autowired
    private PeerService(ConnectionFactory factory) throws IOException, TimeoutException, URISyntaxException, NoSuchAlgorithmException, KeyManagementException {
        executorService = Executors.newCachedThreadPool();

        factory.setUri(URI);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        this.channel = channel;
    }


    public void sendMessage(final Message message) {
        executorService.submit(() -> this.sendMessageInternal(message));
    }

    public void recvMessage(final String userName) {
        executorService.submit(() -> this.recvMessageInternal(userName + "-util"));
        executorService.submit(() -> this.recvMessageInternal(userName + "-chat"));
        this.myUserName = userName;
    }

    private synchronized void sendMessageInternal(final Message message) {

        messages.add(message);

        switch (message.getType()) {
            case MESSAGE:
                handleSendMessage(message);
                break;
            case ACK:
                handleSendAck(message);
                break;
            case REQUEST:
                handleSendRequest(message);
                break;
            case GROUP_REQUEST:
                handleSendGroupRequest(message);
                break;
            case GROUP_ACK:
                handleSendGroupAck(message);
                break;
            case BYE:
                handleSendBye(message);
                break;
            case BYEBYE:
                handleSendByeBye(message);
                break;
            default:
                System.out.println("NOT A VALID COMMAND TO SEND");
        }

    }


    @SneakyThrows
    private synchronized void recvMessageInternal(final String userName) {

        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        final String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, userName);

        final DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            final String response = new String(delivery.getBody(), StandardCharsets.UTF_8);
            final Message message = Message.fromJson(response);

            messages.add(message);

            switch (message.getType()) {
                case MESSAGE: {
                    handleReceiveMessage(message, delivery);
                    break;
                }
                case REQUEST: {
                    handleReceiveRequest(message);
                    break;
                }
                case ACK: {
                    handleReceiveAck(message);
                    break;
                }
                case GROUP_REQUEST: {
                    handleReceiveGroupRequest(message);
                    break;
                }
                case BYE: {
                    handleReceiveBye(message);
                    break;
                }
                default:
                    break;

            }
        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
    }

    private void handleReceiveBye(Message message) {
        if (friends.contains(message.getFrom())) {
            System.out.println(message.getFrom() + " disconnected");
            friends.remove(message.getFrom());
        }
    }

    private void handleReceiveGroupRequest(Message message) {
        System.out.println("You got a group request from: " + message.getFrom());
        groupRequests.add(message.getFrom());
    }

    private void handleReceiveAck(Message message) {
        if (requestsSent.contains(message.getFrom())) {
            requestsSent.remove(message.getFrom());
            friends.add(message.getFrom());
            System.out.println(message.getFrom() + " accepted your friend request");
        }
    }

    private void handleReceiveRequest(Message message) {
        System.out.println("You got a friend request from: " + message.getFrom());
        requestsReceived.add(message.getFrom());
    }

    private void handleReceiveMessage(Message message, Delivery delivery) {
        if (friends.contains(message.getFrom()) || groups.contains(delivery.getEnvelope().getRoutingKey().split("-")[0])) {
            System.out.println(" [x] Received '" +
                    delivery.getEnvelope().getRoutingKey() + "':'" + message + "'");
        }
    }


    public void handleSendMessage(Message message) {
        String queueName = message.getTo() + "-" + "chat";

        if (friends.contains(message.getTo()) || groups.contains(message.getTo())) {
            System.out.println(" [x] Sent to '" + queueName + "':'" + message + "'");
        } else {
            System.out.println(message.getTo() + " is not your friend!");
            return;
        }

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            channel.basicPublish(EXCHANGE_NAME, queueName, null, message.toJson().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleSendRequest(Message message) {
        String queueName = message.getTo() + "-" + "util";

        requestsSent.add(message.getTo());
        System.out.println(" [x] Sent to '" + queueName + "':'" + message + "'");

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            channel.basicPublish(EXCHANGE_NAME, queueName, null, message.toJson().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleSendAck(Message message) {
        String queueName = message.getTo() + "-" + "util";

        if (requestsReceived.contains(message.getTo())) {
            System.out.println(message.getTo() + " is now your friend");
            requestsReceived.remove(message.getTo());
            friends.add(message.getTo());
        } else {
            System.out.println(message.getTo() + " didn't ask for your friendship");
            return;
        }

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            channel.basicPublish(EXCHANGE_NAME, queueName, null, message.toJson().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleSendGroupRequest(Message message) {
        String queueName = message.getTo() + "-" + "util";

        System.out.println(" [x] Sent to '" + queueName + "':'" + message + "'");

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            channel.basicPublish(EXCHANGE_NAME, queueName, null, message.toJson().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleSendGroupAck(Message message) {
        if (groupRequests.contains(message.getTo()) && !groups.contains(message.getTo())) {
            System.out.println("You are now part of the group: " + message.getTo());
            groupRequests.remove(message.getTo());
            addGroup(message.getTo());
        } else {
            System.out.println(message.getTo() + " didn't invite you in");
        }
    }

    public void handleSendBye(Message message) {
        String queueName = message.getTo() + "-" + "util";

        if (friends.remove(message.getTo())) {
            System.out.println("You disconnected with " + message.getTo());
        }

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");
            channel.basicPublish(EXCHANGE_NAME, queueName, null, message.toJson().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void handleSendByeBye(Message message) {
        for (var friend : friends) {
            final Message messageToSend = Message.builder()
                    .from(message.getFrom())
                    .to(friend)
                    .type(Message.Type.BYE)
                    .build();
            sendMessage(messageToSend);
        }

        for (var group : groups) {
            final Message messageToSend = Message.builder()
                    .from(message.getFrom())
                    .to(group)
                    .type(Message.Type.BYE)
                    .build();
            sendMessage(messageToSend);
        }

        System.out.println("Disconnected...");
    }

    public Set<String> getFriends() {
        return friends;
    }

    public void addGroup(String groupName) {
        groups.add(groupName);
        executorService.submit(() -> this.recvMessageInternal(groupName + "-chat"));
    }

    public void startListening(String userName) {
        recvMessage(userName);
    }

    public List<Message> getMessages(String userName) {
        List<Message> userMessages = new ArrayList<>();
        for (var message : messages) {
            if (message.getTo().equals(userName) || (message.getFrom().equals(userName) && message.getTo().equals(this.myUserName))) {
                userMessages.add(message);
            }
        }
        return userMessages;
    }
}

