package com.tora.Controller;

import com.tora.message.Message;
import com.tora.service.PeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class Controller {
    private  String myUsername;
    @Autowired
    PeerService peerService;

    @PostMapping("/hello/{username}")
    public void sendHello(@PathVariable String username) {
        final Message message = Message.builder()
                .from(myUsername)
                .to(username)
                .type(Message.Type.REQUEST)
                .build();
        peerService.sendMessage(message);
    }

    @GetMapping("/messages/{username}")
    public List<Message> getMessages(@PathVariable String username) {
        return this.peerService.getMessages(username);
    }

    @PostMapping("/ack/{username}")
    public void sendAck(@PathVariable String username) {
        final Message message = Message.builder()
                .from(myUsername)
                .to(username)
                .type(Message.Type.ACK)
                .build();
        peerService.sendMessage(message);
    }

    @PostMapping("/login/{username}")
    public void logIn(@PathVariable String username) {
        this.myUsername = username;
        System.out.println("You logged with username: "+username);
        peerService.startListening(username);
    }

    @PostMapping("/group-hello/{groupName}")
    public void sendGroupHello(@PathVariable String groupName, @RequestBody List<String> users) {
        for (String user : users) {
            final Message message = Message.builder()
                    .from(groupName)
                    .to(user)
                    .type(Message.Type.GROUP_REQUEST)
                    .build();
            peerService.sendMessage(message);
        }

        peerService.addGroup(groupName);
    }

    @PostMapping("/group-ack/{groupName}")
    public void sendGroupAck(@PathVariable String groupName) {
        final Message message = Message.builder()
                .from(myUsername)
                .to(groupName)
                .type(Message.Type.GROUP_ACK)
                .build();

        peerService.sendMessage(message);
    }

    @PostMapping("/bye/{username}")
    public void sendBye(@PathVariable String username) {
        final Message message = Message.builder()
                .from(myUsername)
                .to(username)
                .type(Message.Type.BYE)
                .build();
        peerService.sendMessage(message);
    }

    @PostMapping("/byebye")
    public void sendByeBye() {
        final Message message = Message.builder()
                .from(myUsername)
                .type(Message.Type.BYEBYE)
                .build();
        peerService.sendMessage(message);
    }

    @PostMapping("/send/{username}")
    public void sendMessage(@PathVariable String username, @RequestBody String content) {
        Set<String> friends = peerService.getFriends();
        System.out.println("Your friends are:");

        for (var friend : friends) {
            System.out.println("- " + friend);
        }

        final Message message = Message.builder()
                .from(myUsername)
                .to(username)
                .type(Message.Type.MESSAGE)
                .content(content).build();

        peerService.sendMessage(message);
    }

}
