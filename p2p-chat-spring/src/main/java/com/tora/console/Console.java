package com.tora.console;

import com.tora.message.Message;
import com.tora.service.PeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

@ComponentScan("com.tora")
public class Console implements CommandLineRunner {
    private final Scanner scanner = new Scanner(System.in);
    private final PeerService peerService;

    @Autowired
    public Console(PeerService peerService) {
        this.peerService = peerService;
    }

    @Override
    public void run(String... args) {
    }
}
