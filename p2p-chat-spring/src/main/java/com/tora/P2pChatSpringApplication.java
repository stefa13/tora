package com.tora;

import com.tora.console.Console;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
public class P2pChatSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Console.class, args);
    }
}
