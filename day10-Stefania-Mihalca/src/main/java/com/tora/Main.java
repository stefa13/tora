package com.tora;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Main.class.getSimpleName())
                .warmupIterations(3)
                .warmupTime(TimeValue.seconds(5))
                .measurementIterations(5)
                .measurementTime(TimeValue.seconds(6))
                .mode(Mode.Throughput)
                .timeUnit(TimeUnit.SECONDS)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Benchmark
    public void benchmarkSimpleFor(BenchmarkState state) {
        int max = 0;
        for (int i = 1; i <= state.iterations; i++) {
            int aux = i + 1;
            max = max < aux ? aux : max;
        }
    }

    @Benchmark
    public void benchmarkSimpleForWithMaxMethod(BenchmarkState state) {
        int max = 0;
        for (int i = 1; i <= state.iterations; i++) {
            int aux = i + 1;
            max = Math.max(max, aux);
        }
    }

    @Benchmark
    public void benchmarkSimpleForWithAutoboxing(BenchmarkState state) {
        int max = 0;
        for (int i = 1; i <= state.iterations; i++) {
            Integer aux = i + 1;
            max = max < aux ? aux : max;
        }
    }

    @Benchmark
    public void benchmarkStreamSequential(BenchmarkState state) {
        IntStream.rangeClosed(1, state.iterations)
                .map(i -> i + 1)
                .max();
    }

    @Benchmark
    public void benchmarkStreamParallel(BenchmarkState state) {
        IntStream.rangeClosed(1, state.iterations).parallel()
                .map(i -> i + 1)
                .max();
    }

    @State(Scope.Benchmark)
    public static class BenchmarkState {

        @Param({"100", "1000", "10000", "1000000", "50000000"})
        public int iterations;

    }

}
