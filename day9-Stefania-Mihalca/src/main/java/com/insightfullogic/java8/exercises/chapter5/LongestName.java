package com.insightfullogic.java8.exercises.chapter5;

import com.insightfullogic.java8.examples.chapter1.Artist;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class LongestName {

    public static Artist byReduce(List<Artist> artists) {
        return artists.stream()
            .reduce(
                (list, artist) -> (comparing((Artist a) -> a.getName().length()).compare(list, artist) >= 0) ? list : artist
            ).get();
    }

    public static Artist byCollecting(List<Artist> artists) {
        return artists.stream()
            .collect(Collectors.maxBy(comparing((Artist a) -> a.getName().length()))).get();
    }

}
