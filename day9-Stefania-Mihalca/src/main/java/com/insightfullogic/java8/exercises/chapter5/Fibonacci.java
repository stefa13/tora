package com.insightfullogic.java8.exercises.chapter5;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
    private final Map<Integer, Long> map;

    public Fibonacci() {
        this.map = new HashMap<>();
        map.put(0, 0L);
        map.put(1, 1L);
    }

    public long fibonacci(int x) {
        return map.computeIfAbsent(x, val -> fibonacci(val - 1) + fibonacci(val - 2));
    }

}
