package com.insightfullogic.java8.exercises.chapter3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Advanced Exercises Question 1
 */
public class MapUsingReduce {

    public static <I, O> List<O> map(Stream<I> stream, Function<I, O> mapper) {
        return stream.
            reduce(
                new ArrayList<O>(),
                (list, element) -> {
                    ArrayList<O> newList = new ArrayList<>(list);
                    newList.add(mapper.apply(element));
                    return newList;
                },
                (list1, list2) -> {
                    ArrayList<O> newList = new ArrayList<>(list1);
                    newList.addAll(list2);
                    return newList;
                }
            );
    }

}
