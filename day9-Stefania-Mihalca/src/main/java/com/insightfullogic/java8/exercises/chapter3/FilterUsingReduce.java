package com.insightfullogic.java8.exercises.chapter3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Advanced Exercises Question 2
 */
public class FilterUsingReduce {

    public static <I> List<I> filter(Stream<I> stream, Predicate<I> predicate) {
        return stream.
            reduce(
                new ArrayList<I>(),
                (list, element) -> {
                    if (predicate.test(element)) {
                        ArrayList<I> newList = new ArrayList<I>(list);
                        newList.add(element);
                        return newList;
                    }
                    return list;
                },
                (list1, list2) -> {
                    ArrayList<I> newList = new ArrayList<>(list1);
                    newList.addAll(list2);
                    return newList;
                }
            );
    }

}
