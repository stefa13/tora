package com.tora;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Producer extends Thread {
    private BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println("Producer started...");
        Random random = new Random();

        while (true) {
            System.out.println("Producer no of elements: " + queue.size());
            queue.add(queue.size());
            try {
                Thread.sleep(random.nextInt(250));
            } catch (InterruptedException e) {
                System.out.println("Something happened in Producer: " + e.getMessage());
            }
        }
    }
}
