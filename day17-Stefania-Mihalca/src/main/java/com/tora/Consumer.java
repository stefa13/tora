package com.tora;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Consumer extends Thread {
    private final BlockingQueue<Integer> queue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println("Consumer started...");
        Random random = new Random();

        while (true) {
            System.out.println("Consumer no of elements: " + queue.size());
            try {
                queue.take();
                Thread.sleep(random.nextInt(250));
            } catch (InterruptedException e) {
                System.out.println("Something happened in Consumer: " + e.getMessage());
            }
        }
    }
}
