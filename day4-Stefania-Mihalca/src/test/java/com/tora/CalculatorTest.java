package com.tora;

import com.tora.calculator.Calculator;
import com.tora.calculator.Utils;
import com.tora.calculator.operation.Operation;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class CalculatorTest {
    private final Calculator calculator = new Calculator();

    @Test
    public void testGetAvailableOperations() {
        List<Operation> availableOperations = calculator.getAvailableOperations();
        System.out.println(availableOperations);

        assert (availableOperations.size() > 0);
    }

    @Test
    public void testEvaluateExpression() {
        String[] expressionTokens = {"2", "+", "3"};

        double expectedResult = 2 + 3;
        double actualResult = calculator.evaluateExpression(Utils.extractOperation(calculator, expressionTokens), expressionTokens);

        assertEquals(expectedResult, actualResult, 0);
    }
}
