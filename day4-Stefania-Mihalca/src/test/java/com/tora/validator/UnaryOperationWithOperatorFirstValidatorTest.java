package com.tora.validator;

import com.tora.calculator.operation.operations.SquareRoot;
import com.tora.calculator.validator.UnaryOperationWithOperatorFirstValidator;
import com.tora.calculator.validator.Validator;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class UnaryOperationWithOperatorFirstValidatorTest {
    private final Validator validator = new UnaryOperationWithOperatorFirstValidator();

    @Test
    public void testValidateExpressionWhenExpressionIsValid() {
        String[] tokens = {"sqrt", "2"};

        validator.validateExpression(new SquareRoot(), tokens);
    }

    @Test
    public void testValidateExpressionWhenThereAreNotThreeTokens() {
        String[] tokens = {"sqrt"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new SquareRoot(), tokens));
    }

    @Test
    public void testValidateExpressionWhenFirstTokenIsNotAnOperand() {
        String[] tokens = {"sqrt", "2a"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new SquareRoot(), tokens));
    }

    @Test
    public void testValidateExpressionWhenSecondTokenIsNotAnOperator() {
        String[] tokens = {"!@", "2"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new SquareRoot(), tokens));
    }
}
