package com.tora.validator;

import com.tora.calculator.operation.operations.Addition;
import com.tora.calculator.validator.BinaryOperationWithOperatorSecondValidator;
import com.tora.calculator.validator.Validator;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class BinaryOperationWithOperatorSecondValidatorTest {
    private final Validator validator = new BinaryOperationWithOperatorSecondValidator();

    @Test
    public void testValidateExpressionWhenExpressionIsValid() {
        String[] tokens = {"2", "+", "3"};

        validator.validateExpression(new Addition(), tokens);
    }

    @Test
    public void testValidateExpressionWhenThereAreNotThreeTokens() {
        String[] tokens = {"2", "+"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Addition(), tokens));
    }

    @Test
    public void testValidateExpressionWhenFirstTokenIsNotAnOperand() {
        String[] tokens = {"2a", "+", "3"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Addition(), tokens));
    }

    @Test
    public void testValidateExpressionWhenThirdTokenIsNotAnOperand() {
        String[] tokens = {"2", "+", "3h"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Addition(), tokens));
    }

    @Test
    public void testValidateExpressionWhenSecondTokenIsNotAnOperator() {
        String[] tokens = {"2", "~a", "3"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Addition(), tokens));
    }
}
