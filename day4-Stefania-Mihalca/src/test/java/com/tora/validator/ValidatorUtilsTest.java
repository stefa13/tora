package com.tora.validator;

import com.tora.calculator.Calculator;
import com.tora.calculator.operation.operations.Addition;
import com.tora.calculator.validator.ValidatorUtils;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertThrows;

public class ValidatorUtilsTest {
    @Test
    public void testIsOperandWhenTokenIsOperand() {
        String token = "20.23";

        boolean actualResult = ValidatorUtils.isOperand(token);

        assertTrue(actualResult);
    }

    @Test
    public void testIsOperandWhenTokenIsNotOperand() {
        String token = "20.23a";

        boolean actualResult = ValidatorUtils.isOperand(token);

        assertFalse(actualResult);
    }

    @Test
    public void testIsOperatorWhenTokenIsOperator() {
        String token = "+";

        boolean actualResult = ValidatorUtils.isOperator(new Calculator(), token);

        assertTrue(actualResult);
    }

    @Test
    public void testIsOperatorWhenTokenIsNotOperator() {
        String token = "a";

        boolean actualResult = ValidatorUtils.isOperator(new Calculator(), token);

        assertFalse(actualResult);
    }

    @Test
    public void testValidateUserInputWhenInputIsNotValid() {
        String[] inputTokens = {"2a", "3"};

        assertThrows(IllegalArgumentException.class, () -> ValidatorUtils.validateUserInput(new Addition(), inputTokens));
    }
}
