package com.tora.validator;

import com.tora.calculator.operation.operations.Minimum;
import com.tora.calculator.validator.BinaryOperationWithOperatorFirstValidator;
import com.tora.calculator.validator.Validator;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class BinaryOperationWithOperatorFirstValidatorTest {
    private final Validator validator = new BinaryOperationWithOperatorFirstValidator();

    @Test
    public void testValidateExpressionWhenExpressionIsValid() {
        String[] tokens = {"min", "2", "3"};

        validator.validateExpression(new Minimum(), tokens);
    }

    @Test
    public void testValidateExpressionWhenThereAreNotThreeTokens() {
        String[] tokens = {"min", "2"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Minimum(), tokens));
    }

    @Test
    public void testValidateExpressionWhenFirstTokenIsNotAnOperand() {
        String[] tokens = {"min", "2a", "3"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Minimum(), tokens));
    }

    @Test
    public void testValidateExpressionWhenThirdTokenIsNotAnOperand() {
        String[] tokens = {"min", "2", "3a"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Minimum(), tokens));
    }

    @Test
    public void testValidateExpressionWhenSecondTokenIsNotAnOperator() {
        String[] tokens = {"!@", "2", "3"};

        assertThrows(IllegalArgumentException.class, () -> validator.validateExpression(new Minimum(), tokens));
    }
}
