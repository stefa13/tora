package com.tora.operations;

import com.tora.calculator.operation.operations.Subtraction;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SubtractionTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = first - second;
        double actualOutput = new Subtraction().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Subtraction subtraction = new Subtraction();

        String expectedOperator = "-";
        String actualOperator = subtraction.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Subtraction subtraction = new Subtraction();

        String expectedOperationName = "Subtraction";
        String actualOperationName = subtraction.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
