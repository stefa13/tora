package com.tora.operations;

import com.tora.calculator.operation.operations.Addition;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class AdditionTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = first + second;
        double actualOutput = new Addition().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Addition addition = new Addition();

        String expectedOperator = "+";
        String actualOperator = addition.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Addition addition = new Addition();

        String expectedOperationName = "Addition";
        String actualOperationName = addition.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
