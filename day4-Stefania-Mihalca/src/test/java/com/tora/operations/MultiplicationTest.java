package com.tora.operations;

import com.tora.calculator.operation.operations.Multiplication;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MultiplicationTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = first * second;
        double actualOutput = new Multiplication().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Multiplication multiplication = new Multiplication();

        String expectedOperator = "*";
        String actualOperator = multiplication.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Multiplication multiplication = new Multiplication();

        String expectedOperationName = "Multiplication";
        String actualOperationName = multiplication.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
