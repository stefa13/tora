package com.tora.operations;

import com.tora.calculator.operation.operations.Round;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class RoundTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;

        double expectedOutput = Math.round(first);
        double actualOutput = new Round().evaluateOperation(first);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Round round = new Round();

        String expectedOperator = "round";
        String actualOperator = round.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Round round = new Round();

        String expectedOperationName = "Round";
        String actualOperationName = round.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
