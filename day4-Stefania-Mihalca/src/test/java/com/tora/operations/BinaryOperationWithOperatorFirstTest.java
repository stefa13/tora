package com.tora.operations;

import com.tora.calculator.operation.operations.Minimum;
import org.junit.Test;

import java.util.ArrayList;

public class BinaryOperationWithOperatorFirstTest {
    @Test
    public void testGetOperands() {
        String[] tokens = {"min", "2.3", "3.2"};
        Minimum minimum = new Minimum();

        var expectedOutput = new ArrayList<Double>() {{
            add(2.3);
            add(3.2);
        }};
        var actualOutput = minimum.getOperands(tokens);

        assert (expectedOutput.equals(actualOutput));
    }
}
