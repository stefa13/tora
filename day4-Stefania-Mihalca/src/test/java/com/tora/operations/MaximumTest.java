package com.tora.operations;

import com.tora.calculator.operation.operations.Maximum;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MaximumTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = Math.max(first, second);
        double actualOutput = new Maximum().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Maximum maximum = new Maximum();

        String expectedOperator = "max";
        String actualOperator = maximum.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Maximum maximum = new Maximum();

        String expectedOperationName = "Maximum";
        String actualOperationName = maximum.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
