package com.tora.operations;

import com.tora.calculator.operation.operations.Minimum;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MinimumTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = Math.min(first, second);
        double actualOutput = new Minimum().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Minimum minimum = new Minimum();

        String expectedOperator = "min";
        String actualOperator = minimum.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Minimum minimum = new Minimum();

        String expectedOperationName = "Minimum";
        String actualOperationName = minimum.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
