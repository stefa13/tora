package com.tora.operations;

import com.tora.calculator.operation.operations.Division;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThrows;

public class DivisionTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;
        double second = 10.56;

        double expectedOutput = first / second;
        double actualOutput = new Division().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testEvaluateExpressionWhenDividingByZero() {
        double first = 20.23;
        double second = 0;

        double expectedOutput = first / second;

        assertThrows(IllegalArgumentException.class, () -> new Division().evaluateOperation(first, second));
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        Division division = new Division();

        String expectedOperator = "/";
        String actualOperator = division.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        Division division = new Division();

        String expectedOperationName = "Division";
        String actualOperationName = division.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
