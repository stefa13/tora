package com.tora.operations;

import com.tora.calculator.operation.operations.SquareRoot;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SquareRootTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 20.23;

        double expectedOutput = Math.sqrt(first);
        double actualOutput = new SquareRoot().evaluateOperation(first);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        SquareRoot sqrt = new SquareRoot();

        String expectedOperator = "sqrt";
        String actualOperator = sqrt.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        SquareRoot sqrt = new SquareRoot();

        String expectedOperationName = "SquareRoot";
        String actualOperationName = sqrt.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
