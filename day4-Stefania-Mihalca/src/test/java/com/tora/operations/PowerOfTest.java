package com.tora.operations;

import com.tora.calculator.operation.operations.PowerOf;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PowerOfTest {
    @Test
    public void testEvaluateExpressionWithCorrectResult() {
        double first = 2.3;
        double second = 3.1;

        double expectedOutput = Math.pow(first, second);
        double actualOutput = new PowerOf().evaluateOperation(first, second);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetOperatorWithCorrectResult() {
        PowerOf powerOf = new PowerOf();

        String expectedOperator = "^";
        String actualOperator = powerOf.getOperator();

        assertEquals(expectedOperator, actualOperator);
    }

    @Test
    public void testGetOperationNameWithCorrectResult() {
        PowerOf powerOf = new PowerOf();

        String expectedOperationName = "PowerOf";
        String actualOperationName = powerOf.getOperationName();

        assertEquals(expectedOperationName, actualOperationName);
    }
}
