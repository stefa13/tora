package com.tora.operations;

import com.tora.calculator.operation.operations.Addition;
import org.junit.Test;

import java.util.ArrayList;

public class BinaryOperationWithOperatorSecondTest {
    @Test
    public void testGetOperands() {
        String[] tokens = {"2", "+", "3"};
        Addition addition = new Addition();

        var expectedOutput = new ArrayList<Double>() {{
            add(2.0);
            add(3.0);
        }};
        var actualOutput = addition.getOperands(tokens);

        assert (expectedOutput.equals(actualOutput));
    }
}
