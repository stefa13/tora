package com.tora.operations;

import com.tora.calculator.operation.operations.SquareRoot;
import org.junit.Test;

import java.util.ArrayList;

public class UnaryOperationWithOperatorFirstTest {
    @Test
    public void testGetOperands() {
        String[] tokens = {"sqrt", "2.3"};
        SquareRoot sqrt = new SquareRoot();

        var expectedOutput = new ArrayList<Double>() {{
            add(2.3);
        }};
        var actualOutput = sqrt.getOperands(tokens);

        assert (expectedOutput.equals(actualOutput));
    }
}
