package com.tora;

import com.tora.calculator.Calculator;
import com.tora.calculator.Utils;
import com.tora.calculator.operation.Operation;
import com.tora.calculator.operation.operations.Addition;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UtilsTest {
    @Test
    public void testUtilsWhenResultIsCorrect() {
        String[] tokens = {"2", "+", "3.8"};

        Operation expected = new Addition();
        Operation actual = Utils.extractOperation(new Calculator(), tokens);

        assertEquals(expected.getOperator(), actual.getOperator());
        assertEquals(expected.getOperands(tokens), actual.getOperands(tokens));
        assertEquals(expected.getOperationName(), actual.getOperationName());
    }
}
