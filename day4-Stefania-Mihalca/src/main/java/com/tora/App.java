package com.tora;

import com.tora.calculator.Calculator;
import com.tora.calculator.ICalculator;
import com.tora.ui.UI;

public class App {
    public static void main(String[] args) {
        ICalculator calculator = new Calculator();
        UI ui = new UI(calculator);
        ui.run();
    }
}
