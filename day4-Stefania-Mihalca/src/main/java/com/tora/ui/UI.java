package com.tora.ui;

import com.tora.calculator.ICalculator;
import com.tora.calculator.Utils;
import com.tora.calculator.operation.Operation;
import com.tora.calculator.validator.ValidatorUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UI {
    private final ICalculator calculator;
    private final List<Pair<String, Double>> history = new ArrayList<>();

    public UI(ICalculator calculator) {
        this.calculator = calculator;
    }

    private void printAvailableOperations() {
        calculator.getAvailableOperations().forEach(
            operation -> System.out.println('\t' + operation.getOperationName() + '\n')
        );
    }

    private void printMenu() {
        System.out.println("Welcome!");
        System.out.println("Available operations: \n");
        printAvailableOperations();

        System.out.println("Type 'history' to view history of performed operations");

        System.out.println("If you want to exit, type 'exit'");

        System.out.println();
    }


    public void run() {
        printMenu();

        Scanner scanner = new Scanner(System.in);
        String userInput;

        while (true) {
            System.out.print("Your expression: ");
            userInput = scanner.nextLine();

            if (userInput.equals("exit")) {
                break;
            }
            if (userInput.equals("history")) {
                history.forEach(performedOperation -> System.out.println(performedOperation.getLeft() + " = " + performedOperation.getRight()));
            } else {
                try {
                    String[] userInputTokens = splitUserInput(userInput);
                    Operation operation = Utils.extractOperation(calculator, userInputTokens);
                    ValidatorUtils.validateUserInput(operation, userInputTokens);
                    double result = calculator.evaluateExpression(operation, userInputTokens);

                    history.add(new MutablePair<>(userInput, result));

                    System.out.println("Result: " + result + "\n");
                } catch (Exception exception) {
                    System.out.println("An error occurred:\n" + exception.getMessage() + "\n");
                }
            }
        }
        System.out.println("\nBye");
    }

    private String[] splitUserInput(String userInput) {
        return userInput.split(" ");
    }
}

