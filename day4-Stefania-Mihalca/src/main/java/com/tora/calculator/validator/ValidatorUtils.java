package com.tora.calculator.validator;

import com.tora.calculator.ICalculator;
import com.tora.calculator.operation.Operation;

import java.util.Arrays;
import java.util.List;

public class ValidatorUtils {

    public static boolean isOperand(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isOperator(ICalculator calculator, String token) {
        List<Operation> operations = calculator.getAvailableOperations();

        return operations.stream().anyMatch(
            operation -> token.equals(operation.getOperator())
        );
    }

    public static void validateUserInput(Operation operation, String[] userInputTokens) {
        Arrays.stream(userInputTokens).forEach(token -> {
            if (!token.equals(operation.getOperator()) && !isOperand(token)) {
                throw new IllegalArgumentException("'" + token + "' is not an operand.");
            }
        });

        operation.getValidator().validateExpression(operation, userInputTokens);
    }
}
