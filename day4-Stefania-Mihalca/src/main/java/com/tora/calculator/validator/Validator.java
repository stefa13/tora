package com.tora.calculator.validator;

import com.tora.calculator.operation.Operation;

@FunctionalInterface
public interface Validator {
    void validateExpression(Operation operation, String[] tokens);
}
