package com.tora.calculator.validator;

import com.tora.calculator.operation.Operation;

public class UnaryOperationWithOperatorFirstValidator implements Validator {
    @Override
    public void validateExpression(Operation operation, String[] tokens) {
        StringBuilder exceptionMessage = new StringBuilder();

        if (tokens.length != 2) {
            exceptionMessage.append("The expression should have 2 tokens\n");
        }

        if (tokens.length > 0 && !tokens[0].equals(operation.getOperator())) {
            exceptionMessage.append("The second token should be an operator\n");
        }

        if (tokens.length > 1 && !ValidatorUtils.isOperand(tokens[1])) {
            exceptionMessage.append("The first token should be an operand\n");
        }

        if (exceptionMessage.length() != 0) {
            throw new IllegalArgumentException(exceptionMessage.substring(0, exceptionMessage.length() - 1));
        }
    }
}
