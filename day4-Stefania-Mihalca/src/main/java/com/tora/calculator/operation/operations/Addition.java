package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorSecond;

public class Addition extends BinaryOperationWithOperatorSecond {

    public Addition() {
        super("+", "Addition");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return operands[0] + operands[1];
    }
}
