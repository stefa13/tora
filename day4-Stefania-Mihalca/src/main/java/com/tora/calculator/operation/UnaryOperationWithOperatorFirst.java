package com.tora.calculator.operation;

import com.tora.calculator.validator.UnaryOperationWithOperatorFirstValidator;

import java.util.ArrayList;

public abstract class UnaryOperationWithOperatorFirst extends UnaryOperation {

    protected UnaryOperationWithOperatorFirst(String operator, String operationName) {
        super(operator, operationName, new UnaryOperationWithOperatorFirstValidator());
    }

    @Override
    public abstract double evaluateOperation(Double... operands);

    @Override
    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<>() {{
            add(Double.valueOf(tokens[1]));
        }};
    }
}
