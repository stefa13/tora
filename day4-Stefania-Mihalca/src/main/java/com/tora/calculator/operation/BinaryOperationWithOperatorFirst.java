package com.tora.calculator.operation;

import com.tora.calculator.validator.BinaryOperationWithOperatorFirstValidator;

import java.util.ArrayList;

public abstract class BinaryOperationWithOperatorFirst extends BinaryOperation {
    protected BinaryOperationWithOperatorFirst(String operator, String operationName) {
        super(operator, operationName, new BinaryOperationWithOperatorFirstValidator());
    }

    @Override
    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<>() {{
            add(Double.valueOf(tokens[1]));
            add(Double.valueOf(tokens[2]));
        }};
    }
}
