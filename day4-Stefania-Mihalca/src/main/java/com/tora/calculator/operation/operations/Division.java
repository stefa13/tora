package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorSecond;

public class Division extends BinaryOperationWithOperatorSecond {
    public Division() {
        super("/", "Division");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        if (Math.abs(operands[1]) <= 1e-6) {
            throw new IllegalArgumentException("Division by zero.");
        }
        return operands[0] / operands[1];
    }
}
