package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorFirst;

public class Minimum extends BinaryOperationWithOperatorFirst {
    public Minimum() {
        super("min", "Minimum");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return Math.min(operands[0], operands[1]);
    }
}
