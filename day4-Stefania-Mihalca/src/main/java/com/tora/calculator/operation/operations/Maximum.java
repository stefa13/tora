package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorFirst;

public class Maximum extends BinaryOperationWithOperatorFirst {

    public Maximum() {
        super("max", "Maximum");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return Math.max(operands[0], operands[1]);
    }
}
