package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorSecond;

public class Multiplication extends BinaryOperationWithOperatorSecond {
    public Multiplication() {
        super("*", "Multiplication");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return operands[0] * operands[1];
    }
}
