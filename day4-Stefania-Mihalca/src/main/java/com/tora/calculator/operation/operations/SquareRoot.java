package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperationWithOperatorFirst;

public class SquareRoot extends UnaryOperationWithOperatorFirst {
    public SquareRoot() {
        super("sqrt", "SquareRoot");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return Math.sqrt(operands[0]);
    }
}
