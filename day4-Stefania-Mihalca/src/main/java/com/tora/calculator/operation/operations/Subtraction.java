package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorSecond;

public class Subtraction extends BinaryOperationWithOperatorSecond {
    public Subtraction() {
        super("-", "Subtraction");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return operands[0] - operands[1];
    }
}
