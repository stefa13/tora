package com.tora.calculator.operation;

import com.tora.calculator.validator.UnaryOperationWithOperatorFirstValidator;

public abstract class UnaryOperation extends Operation {
    protected UnaryOperation(String operator, String operationName, UnaryOperationWithOperatorFirstValidator validator) {
        super(operator, operationName, validator);
    }
}
