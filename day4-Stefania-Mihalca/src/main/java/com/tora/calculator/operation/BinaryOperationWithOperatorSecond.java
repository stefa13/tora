package com.tora.calculator.operation;

import com.tora.calculator.validator.BinaryOperationWithOperatorSecondValidator;

import java.util.ArrayList;

public abstract class BinaryOperationWithOperatorSecond extends BinaryOperation {
    protected BinaryOperationWithOperatorSecond(String operator, String operationName) {
        super(operator, operationName, new BinaryOperationWithOperatorSecondValidator());
    }

    @Override
    public ArrayList<Double> getOperands(String[] tokens) {
        return new ArrayList<>() {{
            add(Double.valueOf(tokens[0]));
            add(Double.valueOf(tokens[2]));
        }};
    }
}
