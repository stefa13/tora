package com.tora.calculator.operation;

import com.tora.calculator.validator.Validator;

public abstract class BinaryOperation extends Operation {
    protected BinaryOperation(String operator, String operationName, Validator validator) {
        super(operator, operationName, validator);
    }
}
