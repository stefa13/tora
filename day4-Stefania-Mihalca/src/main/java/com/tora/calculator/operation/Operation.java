package com.tora.calculator.operation;

import com.tora.calculator.validator.Validator;

import java.util.ArrayList;

public abstract class Operation {
    private final String operator;
    private final String operationName;
    private final Validator validator;

    protected Operation(String operator, String operationName, Validator validator) {
        this.operator = operator;
        this.operationName = operationName;
        this.validator = validator;
    }

    public String getOperator() {
        return operator;
    }

    public String getOperationName() {
        return operationName;
    }

    public Validator getValidator() {
        return validator;
    }

    public abstract double evaluateOperation(Double... operands);

    public abstract ArrayList<Double> getOperands(String[] tokens);
}
