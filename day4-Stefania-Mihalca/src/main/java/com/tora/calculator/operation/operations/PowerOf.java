package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.BinaryOperationWithOperatorSecond;

public class PowerOf extends BinaryOperationWithOperatorSecond {
    public PowerOf() {
        super("^", "PowerOf");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return Math.pow(operands[0], operands[1]);
    }
}
