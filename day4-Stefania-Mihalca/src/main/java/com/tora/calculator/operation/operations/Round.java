package com.tora.calculator.operation.operations;

import com.tora.calculator.operation.UnaryOperationWithOperatorFirst;

public class Round extends UnaryOperationWithOperatorFirst {
    public Round() {
        super("round", "Round");
    }

    @Override
    public double evaluateOperation(Double... operands) {
        return Math.round(operands[0]);
    }
}
