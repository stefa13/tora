package com.tora.calculator;

import com.tora.calculator.operation.Operation;

import java.util.List;

public interface ICalculator {
    List<Operation> getAvailableOperations();

    double evaluateExpression(Operation operation, String[] operands);
}
