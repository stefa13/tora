package com.tora.calculator;

import com.tora.calculator.operation.Operation;
import com.tora.calculator.validator.ValidatorUtils;

public class Utils {

    public static <operation> Operation extractOperation(ICalculator calculator, String[] expressionTokens) {
        Operation operation = null;

        for (String token : expressionTokens) {
            if (ValidatorUtils.isOperand(token)) {
                continue;
            }
            for (Operation availableOperation : calculator.getAvailableOperations()) {
                if (token.equals(availableOperation.getOperator())) {
                    if (operation == null) {
                        operation = availableOperation;
                    } else {
                        throw new IllegalArgumentException("There should be at most one operator.");
                    }
                }
            }
        }
        if (operation == null) {
            throw new IllegalArgumentException("The expression does not contain any supported operator.");
        }
        return operation;
    }
}
