package com.tora.calculator;

import com.tora.calculator.operation.Operation;
import com.tora.calculator.operation.operations.*;

import java.util.ArrayList;
import java.util.List;

public class Calculator implements ICalculator {
    private final List<Operation> availableOperations =
        new ArrayList<>() {{
            add(new Addition());
            add(new Multiplication());
            add(new Subtraction());
            add(new Division());
            add(new Minimum());
            add(new Maximum());
            add(new PowerOf());
            add(new Round());
            add(new SquareRoot());
        }};

    @Override
    public List<Operation> getAvailableOperations() {
        return availableOperations;
    }

    @Override
    public double evaluateExpression(Operation operation, String[] tokens) {
        var operands = operation.getOperands(tokens);
        return operation.evaluateOperation(operands.toArray(new Double[0]));
    }
}
