package com.tora;

/**
 * Hello world!
 */
public class Add {
    public final static int LOWER_BOUND = 0;

    public final static int UPPER_BOUND = Integer.MAX_VALUE / 2;

    /**
     * @param x - integer
     * @param y - integer
     * @return sum - integer, representing the sum of the 2 given parameters
     * @throws IllegalArgumentException - if at least one of the given parameters are not between the given bounds
     */
    public static int add(int x, int y) {
        if (!(x >= LOWER_BOUND && y >= LOWER_BOUND && x <= UPPER_BOUND && y <= UPPER_BOUND)) {
            throw new IllegalArgumentException("The given parameters are not between the bounds.");
        }

        return x + y;
    }
}
