package com.tora;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class AddTest {
    @Test
    public void addWhenBothParametersAreBetweenBounds() {
        int x = 2;
        int y = 3;

        int expectedOutput = x + y;
        int actualOutput = Add.add(x, y);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void addWhenFirstParameterIsLessThanLowerBound() {
        int x = Add.LOWER_BOUND - 1;
        int y = 3;

        assertThrows(IllegalArgumentException.class, () -> Add.add(x, y));
    }

    @Test
    public void addWhenSecondParameterIsLessThanLowerBound() {
        int x = 2;
        int y = Add.LOWER_BOUND - 1;

        assertThrows(IllegalArgumentException.class, () -> Add.add(x, y));
    }

    @Test
    public void addWhenFirstParameterIsGreaterThanUpperBound() {
        int x = Add.UPPER_BOUND + 1;
        int y = 2;

        assertThrows(IllegalArgumentException.class, () -> Add.add(x, y));
    }

    @Test
    public void addWhenSecondParameterIsGreaterThanUpperBound() {
        int x = 2;
        int y = Add.UPPER_BOUND + 1;

        assertThrows(IllegalArgumentException.class, () -> Add.add(x, y));
    }
}
