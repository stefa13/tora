package com.tora;

public class Task implements Runnable, Comparable<Task> {
    private final Priority priority;
    private final String context;
    private final Runnable runnable;

    public Task(Priority priority, String context, Runnable runnable) {
        this.priority = priority;
        this.context = context;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        this.runnable.run();
    }

    @Override
    public int compareTo(Task o) {
        return this.priority.ordinal() - o.priority.ordinal();
    }

    enum Priority {
        CRITICAL,
        HIGH,
        MEDIUM,
        LOW
    }

    public Priority getPriority() {
        return priority;
    }

    public String getContext() {
        return context;
    }

    @Override
    public String toString() {
        return "Task{" +
            "priority=" + priority +
            ", context='" + context + '\'' +
            ", runnable=" + runnable +
            '}';
    }
}
