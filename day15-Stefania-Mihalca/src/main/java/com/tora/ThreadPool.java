package com.tora;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

public class ThreadPool {
    private final int threads;
    private final Queue<Task> queue;
    private final PoolWorker[] poolWorkers;
    private final List<String> runningContexts;
    private boolean isStopped;

    public ThreadPool(int threads) {
        this.threads = threads;
        this.queue = new PriorityBlockingQueue<>();
        this.poolWorkers = new PoolWorker[threads];
        this.runningContexts = new ArrayList<>();
        this.isStopped = false;

        for (int i = 0; i < threads; ++i) {
            poolWorkers[i] = new PoolWorker();
        }
    }

    public void start() {
        for (int i = 0; i < threads; ++i) {
            poolWorkers[i].start();
        }
    }

    public void stop() throws InterruptedException {
        this.isStopped = true;

        for (int i = 0; i < threads; ++i) {
            poolWorkers[i].join();
            System.out.println("joined thread " + i);
        }
    }

    public void addTask(Task task) {
        synchronized (queue) {
            this.queue.add(task);
            this.queue.notify();
        }
    }

    class PoolWorker extends Thread {

        @Override
        public void run() {
            Task task = null;

            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait(500);
                            if (isStopped && queue.isEmpty()) {
                                return;
                            }
                        } catch (InterruptedException e) {
                            System.out.println("An error occurred while waiting: " + e.getMessage());
                        }
                    }

                    Queue<Task> tasks = new PriorityBlockingQueue<>();

                    boolean foundTask = false;

                    synchronized (runningContexts) {
                        while (!queue.isEmpty()) {
                            task = queue.poll();
//                            System.out.println(task);
//                            System.out.println(Thread.currentThread().getName());

                            if (runningContexts.contains(task.getContext())) {
                                tasks.add(task);
                            } else {
                                foundTask = true;
                                break;
                            }
                        }

                        queue.addAll(tasks);

                        if (task != null && foundTask) {
                            runningContexts.add(task.getContext());
                        } else {
                            task = null;
                        }
                    }
                }

                try {
                    if (task != null) {
                        System.out.printf("Thread %s executing task %s%n", Thread.currentThread().getName(), task);
                        task.run();
                        synchronized (runningContexts) {
                            runningContexts.remove(task.getContext());
                        }
                    } else {
                        synchronized (queue) {
                            queue.wait(500);
                        }
                    }
                } catch (RuntimeException | InterruptedException e) {
                    System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
                }
            }
        }
    }
}
