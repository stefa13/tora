package com.tora;

class TPTester {
    public static void main(String[] args) throws InterruptedException {
        int threads = 2;

        ThreadPool pool = new ThreadPool(threads);

        pool.addTask(new Task(Task.Priority.HIGH, "context 1", () -> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("slept 10s");
        }));

        pool.addTask(new Task(Task.Priority.HIGH, "context 1", () -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("slept 5s");
        }));

        pool.addTask(new Task(Task.Priority.MEDIUM, "context 2", () -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("slept 3s");
        }));

        pool.start();

        pool.stop();
    }
}
