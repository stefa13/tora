package com.tora.benchmark;

import com.tora.domain.Order;
import com.tora.repository.Repository;

import java.util.Random;
import java.util.stream.IntStream;

public class BenchmarkState {
    final public int NUMBER_OF_ELEMENTS = 1000000;
    public Random random;
    public Repository<Order> repository;
    public int currentItem;

    public void setUp(Repository<Order> repo) {
        currentItem = 0;
        random = new Random();
        repository = repo;

        IntStream.range(0, NUMBER_OF_ELEMENTS).forEach(
            value -> repository.add(Order.builder()
                .id(value)
                .price(value)
                .quantity(value)
                .build()
            )
        );
    }

    public void tearDown() {
        System.out.println(repository.size() + " elements at tearDown.");
    }
}
