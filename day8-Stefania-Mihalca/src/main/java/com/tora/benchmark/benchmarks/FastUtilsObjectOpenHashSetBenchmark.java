package com.tora.benchmark.benchmarks;

import com.tora.benchmark.BenchmarkState;
import com.tora.domain.Order;
import com.tora.repository.impl.fastUtils.FastUtilsObjectOpenHashSetRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class FastUtilsObjectOpenHashSetBenchmark {
    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4, time = 3)
    @Measurement(iterations = 5, time = 3)
    public void addFastUtilsOpenHashSetBenchmark(FastUtilsObjectOpenHashSetBenchmark.FastUtilsObjectOpenHashSetState state) {
        int value = state.currentItem + state.NUMBER_OF_ELEMENTS;

        state.repository.add(Order.builder()
            .id(value)
            .price(value)
            .quantity(value)
            .build());
        ++state.currentItem;
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void containsFastUtilsOpenHashSetBenchMark(FastUtilsObjectOpenHashSetBenchmark.FastUtilsObjectOpenHashSetState state) {
        int value = state.random.nextInt(state.NUMBER_OF_ELEMENTS * 2);

        state.repository.contains(Order.builder()
            .id(value)
            .price(value)
            .quantity(value)
            .build());
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4, time = 5)
    @Measurement(iterations = 6, time = 10)
    public void removeFastUtilsOpenHashSetBenchmark(FastUtilsObjectOpenHashSetBenchmark.FastUtilsObjectOpenHashSetState state) {
        int value = state.random.nextInt(state.NUMBER_OF_ELEMENTS);

        state.repository.remove(Order.builder()
            .id(value)
            .price(value)
            .quantity(value)
            .build()
        );
    }

    @State(Scope.Thread)
    public static class FastUtilsObjectOpenHashSetState extends BenchmarkState {
        @Setup(Level.Iteration)
        public void setUp() {
            super.setUp(new FastUtilsObjectOpenHashSetRepository<>());
        }

        @TearDown(Level.Iteration)
        public void tearDown() {
            super.tearDown();
        }
    }
}
