package com.tora.repository.impl.fastUtils;

import com.tora.repository.Repository;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

import java.util.Set;

public class FastUtilsObjectOpenHashSetRepository<T> implements Repository<T> {
    private final Set<T> set;

    public FastUtilsObjectOpenHashSetRepository() {
        this.set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T t) {
        set.add(t);
    }

    @Override
    public boolean contains(T t) {
        return set.contains(t);
    }

    @Override
    public void remove(T t) {
        set.remove(t);
    }

    @Override
    public int size() {
        return set.size();
    }
}
