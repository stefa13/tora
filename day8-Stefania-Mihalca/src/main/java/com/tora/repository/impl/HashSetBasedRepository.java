package com.tora.repository.impl;

import com.tora.repository.Repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements Repository<T> {
    private final Set<T> set;

    public HashSetBasedRepository() {
        this.set = new HashSet<>();
    }

    @Override
    public void add(T t) {
        set.add(t);
    }

    @Override
    public boolean contains(T t) {
        return set.contains(t);
    }

    @Override
    public void remove(T t) {
        set.remove(t);
    }

    @Override
    public int size() {
        return set.size();
    }
}
