package com.tora.repository.impl;

import com.tora.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements Repository<T> {
    private final List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }

    @Override
    public int size() {
        return list.size();
    }
}
