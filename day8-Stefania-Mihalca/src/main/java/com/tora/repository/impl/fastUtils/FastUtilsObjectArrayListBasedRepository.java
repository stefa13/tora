package com.tora.repository.impl.fastUtils;

import com.tora.repository.Repository;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.List;

public class FastUtilsObjectArrayListBasedRepository<T> implements Repository<T> {
    private final List<T> list;

    public FastUtilsObjectArrayListBasedRepository() {
        this.list = new ObjectArrayList<>();
    }

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }

    @Override
    public int size() {
        return list.size();
    }
}
