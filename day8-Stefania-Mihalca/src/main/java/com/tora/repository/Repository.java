package com.tora.repository;

public interface Repository<T> {
    void add(T t);

    boolean contains(T t);

    void remove(T t);

    int size();
}
