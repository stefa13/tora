package com.tora;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class App {

    static void testMap(Map<Integer, Integer> map) {
        Random random = new Random();

        long startPut = System.currentTimeMillis();

        for (int i = 0; i < 1000000; ++i) {
            map.put(random.nextInt(), random.nextInt());
        }

        long endPut = System.currentTimeMillis();

        System.out.println("Time - put: " + (endPut - startPut) + " ms");

        long startGet = System.currentTimeMillis();

        for (int i = 0; i < 1000000; ++i) {
            map.get(i);
        }

        long endGet = System.currentTimeMillis();

        System.out.println("Time - get: " + (endGet - startGet) + " ms");
    }

    static void testSet(Set<Integer> set) {
        Random random = new Random();

        long startAdd = System.currentTimeMillis();

        for (int i = 0; i < 1000000; ++i) {
            set.add(random.nextInt());
        }

        long endAdd = System.currentTimeMillis();

        System.out.println("Time - add: " + (endAdd - startAdd) + " ms");
    }


    public static void main(String[] args) {
        System.out.println("------Hash Map ------");
        testMap(new HashMap<Integer, Integer>());
        System.out.println("------Concurrent Hash Map------");
        testMap(new ConcurrentHashMap<Integer, Integer>());
        System.out.println("------Linked Hash Map------");
        testMap(new LinkedHashMap<Integer, Integer>());
        System.out.println("------Tree Map------");
        testMap(new TreeMap<Integer, Integer>());

        System.out.println();

        System.out.println("------Hash Set------");
        testSet(new HashSet<Integer>());
        System.out.println("------Tree Set------");
        testSet(new TreeSet<Integer>());
        System.out.println("------Linked Hash Set------");
        testSet(new LinkedHashSet<Integer>());
    }
}
