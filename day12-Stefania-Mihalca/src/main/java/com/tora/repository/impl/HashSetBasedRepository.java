package com.tora.repository.impl;

import com.tora.repository.IRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
//@Primary
public class HashSetBasedRepository<T> implements IRepository<T> {
    private final Set<T> set;

    public HashSetBasedRepository() {
        this.set = new HashSet<>();
    }

    @Override
    public void add(T t) {
        set.add(t);
    }

    @Override
    public boolean contains(T t) {
        return set.contains(t);
    }

    @Override
    public void remove(T t) {
        set.remove(t);
    }

    @Override
    public void update(T t1, T t2) {
        set.remove(t1);
        set.add(t2);
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public List<T> getAll() {
        return new ArrayList<>(set);
    }
}
