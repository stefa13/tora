package com.tora.repository.impl;

import com.tora.repository.IRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Primary
public class ArrayListBasedRepository<T> implements IRepository<T> {
    private final List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }

    @Override
    public void update(T t1, T t2) {
        var index = list.indexOf(t1);
        list.set(index, t2);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public List<T> getAll() {
        return list;
    }
}
