package com.tora.repository;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRepository<T> {
    void add(T t);

    boolean contains(T t);

    void remove(T t);

    void update(T t1, T t2);

    int size();

    List<T> getAll();
}
