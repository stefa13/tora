package com.tora.logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class LoggingAspect {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(public void com.tora.repository.IRepository.add(Object))")
    public void addOrderPointCut() {
    }

    @Around("addOrderPointCut()")
    public Object logAddOrder(ProceedingJoinPoint jp) throws Throwable {
        logger.info("add order: " + jp.getArgs()[0]);
        var proceed = jp.proceed();
        logger.info("added order: " + jp.getArgs()[0]);
        return proceed;
    }

    @Pointcut("execution(public void com.tora.repository.IRepository.update(Object, Object))")
    public void updateOrderPointCut() {
    }

    @Around("updateOrderPointCut()")
    public Object logUpdateOrder(ProceedingJoinPoint jp) throws Throwable {
        logger.info("update order: " + jp.getArgs()[0] + "with order: " + jp.getArgs()[1]);
        var proceed = jp.proceed();
        logger.info("updated order");
        return proceed;
    }

    @Pointcut("execution(public void com.tora.repository.IRepository.remove(Object))")
    public void removeOrderPointCut() {
    }

    @Around("removeOrderPointCut()")
    public Object logRemoveOrder(ProceedingJoinPoint jp) throws Throwable {
        logger.info("remove order: " + jp.getArgs()[0]);
        var proceed = jp.proceed();
        logger.info("removed order: " + jp.getArgs()[0]);
        return proceed;
    }
}
