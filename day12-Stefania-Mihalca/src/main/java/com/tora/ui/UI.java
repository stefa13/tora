package com.tora.ui;

import com.tora.domain.Order;
import com.tora.repository.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class UI {
    @Autowired
    private IRepository<Order> repository;

    private void printMenu() {
        System.out.println("1. Add order");
        System.out.println("2. Update order");
        System.out.println("3. Remove order");
        System.out.println("4. List orders");
        System.out.println("5. Exit");
    }

    public void run() {
        printMenu();

        Scanner scanner = new Scanner(System.in);
        String userInput;

        while (true) {
            System.out.print("Your choice: ");
            userInput = scanner.nextLine();

            switch (userInput) {
                case "1": {
                    System.out.println("ID: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Price: ");
                    int price = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Quantity: ");
                    int quantity = scanner.nextInt();
                    scanner.nextLine();

                    repository.add(new Order(id, price, quantity));
                    break;
                }
                case "2": {
                    System.out.println("ID: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Price: ");
                    int price = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Quantity: ");
                    int quantity = scanner.nextInt();
                    scanner.nextLine();

                    repository.update(new Order(id, 0, 0), new Order(id, price, quantity));
                    break;
                }
                case "3": {
                    System.out.println("ID: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();

                    repository.remove(new Order(id, 0, 0));
                    break;
                }
                case "4": {
                    repository.getAll()
                        .forEach(System.out::println);
                    break;
                }
                case "5":
                    return;
            }
        }
    }
}
