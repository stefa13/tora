import {Component, OnInit} from '@angular/core';
import {TrackerService} from "./tracker.service";
import {formatDate} from "@angular/common";
import Swal from 'sweetalert2';
import {Coordinate} from "./coordinate";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'ISS Tracker';
  passes: any;
  clicked: boolean = false;
  passesCount: any;
  weatherList: any;

  zoom: number = 6.5;

  // @ts-ignore
  myLocation: Coordinate = new Coordinate(null, null, null);

  // @ts-ignore
  initialLocation: Coordinate = new Coordinate(null, null, null);

  // @ts-ignore
  markLocation: Coordinate = new Coordinate(null, null, null);

  constructor(private service: TrackerService) {
  }

  ngOnInit(): void {
    this.initialLocation = new Coordinate(45.69972244458541, 25.02693487169206, 0);
    this.markLocation = new Coordinate(45.69972244458541, 25.02693487169206, 0);

    this.getMyLocation();
  }

  handleMapClick(event: any): any {
    this.clicked = true;

    const {lat, lng} = event.coords;
    this.markLocation.setLat(lat);
    this.markLocation.setLong(lng);

    this.getWeather(this.markLocation.getLat(), this.markLocation.getLong());

    this.service
      .getElevationByLatAndLong(this.markLocation.getLat(), this.markLocation.getLong())
      .subscribe(result => {
        this.markLocation.setAlt(result.results[0].elevation);
        this.getISSVisualPasses(this.markLocation.getLat(), this.markLocation.getLong(), this.markLocation.getAlt());
      });
  }

  saveMyLocation() {
    this.myLocation = this.markLocation;

    this.service
      .saveMyLocation(this.myLocation.getLat(), this.myLocation.getLong(), this.myLocation.getAlt())
      .subscribe(
        _ => {
          Swal.fire({
            title: 'Success!',
            text: 'Your location was saved',
            icon: 'success',
            confirmButtonText: 'OK'
          }).then();
        },
        _ => {
          Swal.fire({
            title: 'Oof!',
            text: 'Your location could not be saved',
            icon: 'error',
            confirmButtonText: 'OK'
          }).then();
        }
      );
  }

  getMyLocation() {
    this.service
      .getMyLocation()
      .subscribe(result => {
        this.myLocation.setLat(parseFloat(result[0]));
        this.myLocation.setLong(parseFloat(result[1]));
        this.myLocation.setAlt(parseFloat(result[2]));
      })
  }

  getWeather(lat: number, long: number) {
    this.service
      .getWeather(lat, long)
      .subscribe(result => this.weatherList = result.list);
  }

  showPassesForMyLocation() {
    if (this.myLocation.getLat() == null) {
      Swal.fire({
        title: 'Error!',
        text: 'Please save your location first',
        icon: 'error',
        confirmButtonText: 'OK'
      }).then();
      return;
    }

    this.clicked = true;

    this.markLocation.setLat(this.myLocation.getLat());
    this.markLocation.setLong(this.myLocation.getLong());

    this.initialLocation.setLat(this.markLocation.getLat());
    this.initialLocation.setLong(this.markLocation.getLong());

    this.zoom = 5;

    this.getWeather(this.myLocation.getLat(), this.myLocation.getLong());

    this.getISSVisualPasses(this.myLocation.getLat(), this.myLocation.getLong(), this.myLocation.getAlt())
  }

  getBestMatchingWeatherDay(date: number) {
    return this.weatherList
      .reduce(
        (a: any, b: any) => Math.abs(a.dt - date) < Math.abs(b.dt - date) ? a : b
      );
  }

  getISSVisualPasses(lat: number, long: number, alt: number) {
    this.service
      .getSatelliteVisualPasses(lat, long, alt)
      .subscribe(result => {
        this.passesCount = result.info.passescount;
        this.passes = result.passes;
      })
  }

  formatDate(utc: number) {
    return formatDate(utc * 1000, "medium", "en-us");
  }

}
