import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TrackerService {
  googleApiKey = 'AIzaSyCG4u6e2zHJB0Q12IKZlnyqR86vgEA9dTg'
  n2YoApiKey = 'FEPLHW-M53MKD-K5PSBJ-4RFX'
  backEndUrl = 'http://localhost:8080/'
  elevationUrl = this.backEndUrl + 'elevation/'
  visualPassesUrl = this.backEndUrl + 'visualpasses/'
  saveMyLocationUrl = this.backEndUrl + 'saveMyLocation/'
  getMyLocationUrl = this.backEndUrl + 'getMyLocation/'
  getWeatherUrl = this.backEndUrl + 'weather/'

  constructor(private http: HttpClient) {
  }

  getElevationByLatAndLong(lat: number, long: number): Observable<any> {
    return this.http.get(this.elevationUrl + lat + "/" + long);
  }

  getSatelliteVisualPasses(lat: number, long: number, alt: number): Observable<any> {
    return this.http.get(this.visualPassesUrl + lat + "/" + long + "/" + alt);
  }

  saveMyLocation(lat: number, long: number, alt: number): Observable<any> {
    return this.http.get(this.saveMyLocationUrl + lat + "/" + long + "/" + alt);
  }

  getMyLocation(): Observable<any> {
    return this.http.get(this.getMyLocationUrl);
  }

  getWeather(lat: number, long: number): Observable<any> {
    return this.http.get(this.getWeatherUrl + lat + "/" + long);
  }
}
