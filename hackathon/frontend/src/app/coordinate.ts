export class Coordinate {
  private lat?: number;
  private long?: number;
  private alt?: number;

  constructor(lat: number, long: number, alt: number) {
    this.lat = lat;
    this.long = long;
    this.alt = alt;
  }

  getLat(): number {
    return <number>this.lat;
  }

  setLat(value: number) {
    this.lat = value;
  }

  getLong(): number {
    return <number>this.long;
  }

  setLong(value: number) {
    this.long = value;
  }

  getAlt(): number {
    return <number>this.alt;
  }

  setAlt(value: number) {
    this.alt = value;
  }
}

