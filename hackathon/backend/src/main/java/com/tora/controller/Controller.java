package com.tora.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {
    private final static String googleAPIKey = "AIzaSyCG4u6e2zHJB0Q12IKZlnyqR86vgEA9dTg";
    private final static String n2YoAPIKey = "FEPLHW-M53MKD-K5PSBJ-4RFX";
    private final static String weatherAPIKey = "20a8481b609d1e654f4eaee59d84c968&fbclid=IwAR24Ob7fAbg-vd92VHaPRiFKPl9VrnsyNy8fW8S735ZmV37IIvQI7tj8fiA";
    private final static String n2YoUrl = "https://api.n2yo.com/rest/v1/satellite/visualpasses/25544/";
    private final static String weatherUrl = "http://api.openweathermap.org/data/2.5/forecast?lat=";
    private final static String googleUrl = "https://maps.googleapis.com/maps/api/elevation/json?locations=";


    @GetMapping("/elevation/{lat}/{lng}")
    public ResponseEntity<?> getElevation(@PathVariable String lat, @PathVariable String lng) throws IOException {
        URL url = new URL(googleUrl + lat + "," + lng + "&key=" + googleAPIKey);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder response = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            response.append(line);
            response.append("\n");
        }

        bufferedReader.close();

        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

    @GetMapping("/visualpasses/{lat}/{lng}/{alt}")
    public ResponseEntity<?> getVisualPasses(@PathVariable String lat, @PathVariable String lng, @PathVariable String alt) throws IOException {
        URL url = new URL(n2YoUrl + lat + "/" + lng + "/" + alt + "/10/100/" + "&apiKey=" + n2YoAPIKey);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder response = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            response.append(line);
            response.append("\n");
        }

        bufferedReader.close();

        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

    @GetMapping("/weather/{lat}/{lng}")
    public ResponseEntity<?> getWeather(@PathVariable String lat, @PathVariable String lng) throws IOException {
        URL url = new URL(weatherUrl + lat + "&lon=" + lng + "&appid=" + weatherAPIKey);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder response = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            response.append(line);
            response.append("\n");
        }

        bufferedReader.close();

        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

    @GetMapping("/saveMyLocation/{lat}/{lng}/{alt}")
    public ResponseEntity<?> saveMyLocation(@PathVariable String lat, @PathVariable String lng, @PathVariable String alt) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("myLocation.txt"))) {
            bufferedWriter.write(lat + "\n");
            bufferedWriter.write(lng + "\n");
            bufferedWriter.write(alt);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/getMyLocation")
    public ResponseEntity<?> getMyLocation() throws IOException {
        String line;
        List<String> coordinates = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("myLocation.txt"))) {
            coordinates.add(bufferedReader.readLine());
            coordinates.add(bufferedReader.readLine());
            coordinates.add(bufferedReader.readLine());
        }

        return new ResponseEntity<>(coordinates, HttpStatus.OK);
    }
}
