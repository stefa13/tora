package com.tora;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Utils {
    public static void printLinesContainingString(String string) throws IOException { // 60369 ms
        Files
            .lines(Path.of("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log"))
            .parallel()
            .filter(line -> line.contains(string))
            .forEach(System.out::println);
    }

    public static void printLinesContainingStringV3(String string) throws IOException { // 54535 ms
        try (LineIterator it = FileUtils.lineIterator(new File("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log"))) {
            while (it.hasNext()) {
                String line = it.nextLine();
                if (line.contains(string)) {
                    System.out.println(line);
                }
            }
        }
    }

    public static void countOccurrencesOfString(String string) throws IOException {
        int count = 0;

        try (LineIterator it = FileUtils.lineIterator(new File("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log"))) {
            while (it.hasNext()) {
                String line = it.nextLine();

                count += getNoOfOccurrences(string, line);
            }
            System.out.println("No. of occurrences: " + count);
        }
    }


    public static int getNoOfOccurrences(String string, String line) {
        return StringUtils.countMatches(line, string);
    }

    public static void mostFrequentlyUsedSubjects() throws IOException {
        Path path = Paths.get("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log");

        var mostFrequentSubject = Files.lines(path)
            .filter(line -> line.contains("subject="))
            .map(line -> StringUtils.substringBetween(line, "subject=", ","))
            .filter(Objects::nonNull)
            .collect(Collectors.groupingBy(line -> line, Collectors.counting()))
            .entrySet()
            .stream()
            .max(Map.Entry.comparingByValue())
            .orElseThrow();

        System.out.println("Most frequent subject: " + mostFrequentSubject.getKey());
        System.out.println("Frequency: " + mostFrequentSubject.getValue());
    }

    public static void mostFrequentlyUsedWords() throws IOException {
        Map<String, Integer> frequencies = new HashMap<>();
        String mostFrequentWord = "";
        int maxCount = 0;

        try (LineIterator it = FileUtils.lineIterator(new File("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log"))) {
            while (it.hasNext()) {
                String line = it.nextLine();
                var words = line.split("[^a-zA-Z0-9]+");
                for (var word : words) {
                    if (!frequencies.containsKey(word)) {
                        frequencies.put(word, 1);
                    } else {
                        frequencies.put(word, frequencies.get(word) + 1);
                    }
                }
            }
        }

        for (var entry : frequencies.entrySet()) {
            if (entry.getValue() > maxCount) {
                maxCount = entry.getValue();
                mostFrequentWord = entry.getKey();
            }
        }

        System.out.println("Most frequent word: " + mostFrequentWord);
        System.out.println("Frequency: " + maxCount);
    }

    public static void splitFile(String filePath, int numberOfThreads) {
        long size = new File(filePath).length();
        System.out.println("Size:" + size);

        final long numberOfBytesPerThread = size / numberOfThreads;
        System.out.println("No bytes/thread: " + numberOfBytesPerThread);

        final List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < numberOfThreads; ++i) {
            int finalI = i;
            final Thread thread = new Thread(() -> {
                try {
                    readPartOfFile(
                        filePath,
                        finalI + 1,
                        (finalI * numberOfBytesPerThread),
                        finalI < numberOfThreads - 1 ? (finalI + 1) * numberOfBytesPerThread : size
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            threads.add(thread);
        }

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private static void readPartOfFile(String filePath, int threadNumber, long startPosition, long endPosition) throws IOException {
        final int CHUNK_SIZE = 1024 * 1024 * 32;
        File file = new File(filePath);
        long bytesRemainingToRead = endPosition - startPosition;
        try (
            FileOutputStream fileOutputStream = new FileOutputStream("D:\\trilava-2021-07-05-POST.log\\a" + "-" + threadNumber + ".txt");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")
        ) {
            randomAccessFile.seek(startPosition);
            while (randomAccessFile.getFilePointer() < endPosition) {
                int bufferSize = (int) Math.min(bytesRemainingToRead, CHUNK_SIZE);
                bytesRemainingToRead -= bufferSize;
                byte[] buffer = new byte[bufferSize];
                randomAccessFile.read(buffer);
                bufferedOutputStream.write(buffer);
            }
        }
    }

    public static void readFrom4FilesWithNThreads(int numberOfThreads, String text) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        List<String> matchingLines = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; ++i) {
            System.out.printf("Starting thread %d for processing.%n", i + 1);
            final int finalI = i;
            executorService.execute(() -> {
                try {
                    threadTask(matchingLines, text, String.format("D:\\trilava-2021-07-05-POST.log\\a-%d.txt", finalI + 1), latch);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        latch.await();

        executorService.shutdown();

        try (final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"))) {
            System.out.printf("Found %d matching lines:%n", matchingLines.size());
            bufferedWriter.write(String.format("Found %d matching lines:%n", matchingLines.size()));
            matchingLines.forEach(line -> {
                System.out.println(line);
                try {
                    bufferedWriter.write(line + '\n');
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void threadTask(List<String> matchingLines, String text, final String filePath, CountDownLatch latch) throws InterruptedException {
        try (final LineIterator it = FileUtils.lineIterator(new File(filePath))) {
            String line;
            while (it.hasNext()) {
                line = it.nextLine();

                if (line.contains(text)) {
                    matchingLines.add(line);
                }
            }

        } catch (final IOException e) {
            e.printStackTrace();
        }

        latch.countDown();
    }


    public static void replaceTextPattern(String pattern, String replacement) throws IOException {
        try (LineIterator it = FileUtils.lineIterator(new File("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\trilava-2021-07-05-POST.log\\newFile.log"))) {
            while (it.hasNext()) {
                String line = it.nextLine();
                if (line.contains(pattern)) {
                    String newLine = line.replaceAll(pattern, replacement);
                    bufferedWriter.write(newLine);
                }
            }
        }
    }

}

