package com.tora;

import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        var start = System.currentTimeMillis();
//        Utils.replaceTextPattern("subject", "newSubject");
//        Utils.mostFrequentlyUsedWords_V2("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log");
        Utils.splitFile("D:\\trilava-2021-07-05-POST.log\\trilava-2021-07-05-POST.log", 4);
//        Utils.readFrom4FilesWithNThreads(4, "POLYMER.scalar");
//        Utils.printLinesContainingStringV3("");
        var end = System.currentTimeMillis();
        System.out.println("Time: " + (end - start));
    }

}
